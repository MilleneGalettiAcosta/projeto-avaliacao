package com.memory.api.medicamentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestMedicamentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestMedicamentosApplication.class, args);
	}

}
